# Equilibrator Cache

[![Join the chat at https://gitter.im/equilibrator-devs/equilibrator-api](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/equilibrator-devs/equilibrator-api?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

An indexed database of compounds from the [MetaNetX](https://www.metanetx.org/)
reference database ([version
3.1](ftp://ftp.vital-it.ch/databases/metanetx/MNXref/3.1/)). Structures are
enriched with information using [Open Babel](http://openbabel.org/) and
[ChemAxon](https://chemaxon.com/).

## Copyright

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

## Disclaimer

MNXref uses information on cellular compartments, reactions, and metabolites that is sourced from a number of external resources. The licensing agreements of those resources are specified in each of the downloadable files listed in the README.md of the FTP server. For each compound, reaction and cellular compartment in the MNXref namespace we indicate which external resource provided the information used in MNXref. Compounds and reactions in the MNXref namespace may be identical to, or differ from, those in the external resource. In either case the data from MNXref may be considered to be subject to the original licensing restrictions of the external resource.

