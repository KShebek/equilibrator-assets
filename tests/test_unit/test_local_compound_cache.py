"""Tests for the local compound cache."""
# The MIT License (MIT)
#
# Copyright (c) 2020 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from pathlib import Path
from sys import platform

import pandas as pd
import pytest

from equilibrator_assets.chemaxon import get_chemaxon_status
from equilibrator_assets.local_compound_cache import LocalCompoundCache


CHEMAXON_STATUS = get_chemaxon_status()
cxcalc = pytest.mark.skipif(
    CHEMAXON_STATUS == 1, reason="cxcalc is not installed."
)
license = pytest.mark.skipif(
    CHEMAXON_STATUS == 2, reason="cxcalc found no ChemAxon license."
)
macOS = pytest.mark.skipif(
    platform == "darwin", reason="cxcalc does not support InChi on macOS."
)


def chemaxon(f):
    """Compose chemaxon requirements into one decorator."""
    return cxcalc(license(f))


@pytest.fixture(scope="function")
def local_cache() -> LocalCompoundCache:
    """Create a local cache."""
    local_cache = LocalCompoundCache()
    local_cache.generate_local_cache_from_default_zenodo(
        "./compounds.sqlite", force_write=True
    )
    local_cache.load_cache("./compounds.sqlite")
    yield local_cache
    # Delete copy of .sqlite file
    local_cache.ccache.session.close()
    local_cache.ccache_path.unlink()
    if Path("compound_creation_log.tsv").is_file():
        Path("compound_creation_log.tsv").unlink()


def test_generate_cache_from_default_zenodo() -> None:
    """Test copying the default cache."""
    local_cache = LocalCompoundCache()
    local_cache.generate_local_cache_from_default_zenodo(
        "compounds.sqlite", force_write=True
    )

    assert Path("compounds.sqlite").is_file()
    # Remove copied database
    Path("compounds.sqlite").unlink()


@chemaxon
def test_get_compounds_from_smiles(local_cache) -> None:
    """Get a mix of compounds that exist and are not in the ccache."""
    smiles = [
        "CC(=O)[O-]",  # Acetate in ccache
        "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",  # 3B4HA Not in ccache
    ]
    compound_list = local_cache.get_compounds(smiles)

    assert compound_list[0].id == 28
    assert compound_list[1].id == 694325

    assert compound_list[0].dissociation_constants == [4.54]
    assert compound_list[1].dissociation_constants == [8.92, 4.23]

    assert len(compound_list[0].magnesium_dissociation_constants) == 1
    assert len(compound_list[1].magnesium_dissociation_constants) == 0


@chemaxon
def test_get_single_compound_from_smiles(local_cache) -> None:
    """Get a single compound."""
    smiles = "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1"  # 3B4HA Not in ccache
    compound = local_cache.get_compounds(smiles)

    assert compound.id == 694325
    assert compound.dissociation_constants == [8.92, 4.23]


@macOS
@chemaxon
def test_get_compounds_from_inchi(local_cache) -> None:
    """Get a mix of compounds that exist and are not in the ccache."""
    inchi = [
        "InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)/p-1",  # Acetate--In ccache
        (
            "InChI=1S/C14H11NO4"
            "/c16-12-7-6-10(14(18)19)8-11(12)15-13(17)9-4-2-1-3-5-9"
            "/h1-8,16H,(H,15,17)(H,18,19)"
        ),  # 3B4HA--Not in ccache
    ]
    compound_list = local_cache.get_compounds(inchi, mol_format="inchi")

    assert compound_list[0].id == 28
    assert compound_list[1].id == 694325

    assert compound_list[0].dissociation_constants == [4.54]
    assert compound_list[1].dissociation_constants == [8.92, 4.23]

    assert len(compound_list[0].magnesium_dissociation_constants) == 1
    assert len(compound_list[1].magnesium_dissociation_constants) == 0


@chemaxon
def test_get_compounds_specifying_pkas(local_cache) -> None:
    """Specify dissociation constants of a compound."""
    smiles = [
        "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",  # 3B4HA Not in ccache
        "OCC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",
    ]
    pka_dict = {"OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1": [4, 5.5, 6]}
    compound_list = local_cache.get_compounds(smiles, specified_pkas=pka_dict)

    assert compound_list[0].dissociation_constants == [4, 5.5, 6]
    assert compound_list[1].dissociation_constants == [13.86, 6.97]


@chemaxon
def test_get_compounds_with_errors(local_cache):
    """Call get_compounds with structures that have errors and test logs."""
    smiles = [
        # Decomposed with chemaxon
        "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",
        # Decomposed with bypass_chemaxon
        "C1(CC(OC(C(C1)=O)CO)O)=O",
        # Cannot be decomposed
        "CC(=O)OC1C=C2C3Cc4ccc(c(c4C2(CCN3C)C=C1OC)O)OC",
    ]
    log_df = pd.DataFrame()
    compounds_without_fails = local_cache.get_compounds(
        smiles, error_log="compound_creation_log.tsv", log_df=log_df
    )
    compounds_with_fails = local_cache.get_compounds(smiles, return_fails=True)

    log_file = pd.read_csv("compound_creation_log.tsv", sep="\t", index_col=[0])

    # Without fails returns only valid compounds
    assert len(compounds_without_fails) == 1

    assert compounds_without_fails[0].id == 694325
    assert compounds_without_fails[0].dissociation_constants == [8.92, 4.23]

    # With fails returns None for failed compounds
    assert len(compounds_with_fails) == 3

    assert compounds_with_fails[0].id == 694325
    assert compounds_with_fails[0].dissociation_constants == [8.92, 4.23]

    assert not any(compounds_with_fails[1:])

    assert all(log_file == log_df)
    assert log_file.loc[0].to_list() == (
        [
            "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",
            "RKCVLDMDZASBEO-UHFFFAOYSA-N",
            "chemaxon",
            "valid",
        ]
    )
    assert log_file.loc[1].to_list() == (
        [
            "C1(CC(OC(C(C1)=O)CO)O)=O",
            "UBJAOLUWBPQQJC-UHFFFAOYSA-N",
            "bypass",
            "failed",
        ]
    )
    assert log_file.loc[2].to_list() == (
        [
            "CC(=O)OC1C=C2C3Cc4ccc(c(c4C2(CCN3C)C=C1OC)O)OC",
            "DNOMLUPMYHAJIY-UHFFFAOYSA-N",
            "empty",
            "failed",
        ]
    )


@chemaxon
def test_get_compounds_bypass_chemaxon(local_cache):
    """Test generating compounds by bypassing chemaxon."""
    smiles = [
        # Decomposed with chemaxon
        "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",
        # Decomposed with bypass_chemaxon
        "C1(CC(OC(C(C1)=O)CO)O)=O",
        # Cannot be decomposed
        "CC(=O)OC1C=C2C3Cc4ccc(c(c4C2(CCN3C)C=C1OC)O)OC",
    ]

    compounds = local_cache.get_compounds(
        smiles, return_fails=True, bypass_chemaxon=True
    )

    assert compounds[0].group_vector
    assert compounds[0].smiles == "OC1=C(NC(=O)C2=CC=CC=C2)C=C(C=C1)C([O-])=O"

    assert compounds[1].group_vector
    assert compounds[1].smiles == "C1(=O)CC(OC(C(=O)C1)CO)O"

    assert not compounds[2]


@chemaxon
def test_get_compounds_save_empty(local_cache):
    """Test saving empty compounds."""
    smiles = [
        # Decomposed with chemaxon
        "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",
        # Decomposed with bypass_chemaxon
        "C1(CC(OC(C(C1)=O)CO)O)=O",
        # Cannot be decomposed
        "CC(=O)OC1C=C2C3Cc4ccc(c(c4C2(CCN3C)C=C1OC)O)OC",
    ]

    compounds = local_cache.get_compounds(
        smiles, return_fails=True, save_empty_compounds=True
    )

    assert compounds[0].group_vector
    assert compounds[0].smiles == "OC1=C(NC(=O)C2=CC=CC=C2)C=C(C=C1)C([O-])=O"

    assert not compounds[1]

    assert not compounds[2].group_vector
    assert compounds[2].smiles == (
        "CC(=O)OC1C=C2C3Cc4ccc(c(c4C2(CCN3C)C=C1OC)O)OC"
    )


def test_get_compounds_read_only(local_cache) -> None:
    """Test read only mode of local cache."""
    local_cache.read_only = True
    smiles = [
        "CC(=O)[O-]",  # Acetate in ccache
        "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",  # 3B4HA Not in ccache
    ]
    log_df = pd.DataFrame()

    compounds = local_cache.get_compounds(smiles, log_df=log_df)

    # Returns valid, known compound
    assert len(compounds) == 1
    assert compounds[0].smiles == "CC([O-])=O"

    # Second compound fails due to read-only
    assert log_df.loc[1, "method"] == "read-only"


@chemaxon
def test_add_compounds(local_cache) -> None:
    """Add compounds with different values provided."""
    smiles = [
        "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",
        "CCO",
        "OC(=O)C1=CCC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",
        "OC(=O)C1=CCCC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",
        "OC(=O)C1=CCCCC(NC(=O)C2=CC=CC=C2)=C(O)C=C1",
    ]

    data = [
        ["OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1", "3B4HA", "test_name"],
        ["CCO", None, "EtOH"],
        ["OC(=O)C1=CCC(NC(=O)C2=CC=CC=C2)=C(O)C=C1", "test_cpd", None],
        ["OC(=O)C1=CCCC(NC(=O)C2=CC=CC=C2)=C(O)C=C1", None, "test_cpd2_name"],
        ["OC(=O)C1=CCCCC(NC(=O)C2=CC=CC=C2)=C(O)C=C1", None, None],
    ]
    compound_df = pd.DataFrame(data=data, columns=["struct", "coco_id", "name"])

    local_cache.add_compounds(compound_df)

    compounds = local_cache.get_compounds(smiles)

    assert len(compounds[0].identifiers) == 2
    assert compounds[0].identifiers[0].registry.namespace == "coco"
    assert compounds[0].identifiers[0].accession == "3B4HA"
    assert compounds[0].identifiers[1].registry.namespace == "synonyms"
    assert compounds[0].identifiers[1].accession == "test_name"
    assert compounds[0].get_common_name() == "test_name"

    assert compounds[1].identifiers[-2].registry.namespace == "synonyms"
    assert compounds[1].identifiers[-2].accession == "etoh"
    assert compounds[1].identifiers[-1].registry.namespace == "coco"
    assert compounds[1].identifiers[-1].accession == "EtOH"
    assert compounds[1].get_common_name() == "Ethanol"

    assert len(compounds[2].identifiers) == 2
    assert compounds[2].identifiers[0].registry.namespace == "coco"
    assert compounds[2].identifiers[0].accession == "test_cpd"
    assert compounds[2].get_common_name() == "test_cpd"

    assert compounds[3].identifiers[0].registry.namespace == "coco"
    assert compounds[3].identifiers[0].accession == "test_cpd2_name"
    assert compounds[3].get_common_name() == "test_cpd2_name"

    assert compounds[4].identifiers[0].registry.namespace == "synonyms"
    assert compounds[4].get_common_name() == "694328"


def test_add_compounds_read_only(local_cache) -> None:
    """Test adding compounds in read-only mode."""
    local_cache.read_only = True
    data = [
        ["OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1", "3B4HA", "testme"],
        ["CCO", None, "EtOH"],
    ]
    compound_df = pd.DataFrame(data=data, columns=["struct", "coco_id", "name"])

    local_cache.add_compounds(compound_df)

    compounds = local_cache.get_compounds(compound_df["struct"].to_list())
    cocoEtOH = compounds[0]
    identifier = cocoEtOH.identifiers[-1]

    assert identifier.registry.namespace == "coco"
    assert identifier.accession == "EtOH"


@chemaxon
def test_local_cache_persistence(local_cache) -> None:
    """Test additions to ccache persist upon reloading."""
    smiles = "OC(=O)C1=CC(NC(=O)C2=CC=CC=C2)=C(O)C=C1"  # 3B4HA Not in ccache
    local_cache.get_compounds(smiles)
    # Disconnect and reload to the database
    local_cache.load_cache(local_cache.ccache_path)
    compound = local_cache.get_compounds(smiles)

    # ID of first inserted compound
    assert compound.id == 694325


def test_get_coco_accessions(local_cache) -> None:
    """Get accessions from coco namespace."""
    accessions = local_cache.get_coco_accessions()

    assert len(accessions) == 13
